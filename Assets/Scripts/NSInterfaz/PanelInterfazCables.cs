﻿using NSPuntaDetectora;
using UnityEngine;

namespace NSInterfaz
{
    public class PanelInterfazCables : AbstractSingletonPanelUIAnimation<PanelInterfazCables>
    {
        [SerializeField] private PuntoMedicionPuntaDetectora[] arrayPuntosMedicionPuntaDetectora;

        private void OnEnable()
        {
            ActivarPuntosTensionCable(false);
        }

        public void SetTensionCables(bool argCable1, bool argCable2, bool argCable3, bool argCable4)
        {
            ActivarPuntosTensionCable();
            arrayPuntosMedicionPuntaDetectora[0]._tieneTension = argCable1;
            arrayPuntosMedicionPuntaDetectora[1]._tieneTension = argCable2;
            arrayPuntosMedicionPuntaDetectora[2]._tieneTension = argCable3;
            arrayPuntosMedicionPuntaDetectora[3]._tieneTension = argCable4;
        }

        public void ActivarPuntosTensionCable(bool argActivar = true)
        {
            for (int i = 0; i < arrayPuntosMedicionPuntaDetectora.Length; i++)
                arrayPuntosMedicionPuntaDetectora[i].gameObject.SetActive(argActivar);
        }
    } 
}
