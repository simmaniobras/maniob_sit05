﻿using NSEvaluacion;
using NSSeguridad;
using System;
using System.Collections;
using System.Collections.Generic;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.UI;

namespace NSCreacionPDF
{
   /// <summary>
   /// Clase encargada de capturar en imagen todas las hojas del PDF
   /// </summary>
   public class CanvasReportePDF : MonoBehaviour
   {
      #region members

      /// <summary>
      /// Camara que hace la captura
      /// </summary>
      [SerializeField] private Camera cameraCapturaPDF;

      /// <summary>
      /// Render texture en donde quedan capturas las imagenes de cada hoja
      /// </summary>
      [SerializeField] private RenderTexture renderTexturePDF;

      /// <summary>
      /// Puntero a la courutina que captura las imagenes del PDF
      /// </summary>
      private IEnumerator couCapturarImagenPDF;

      /// <summary>
      /// Array de texturas que son las imagenes de las hojas capturadas
      /// </summary>
      private Texture2D[] capturaHojasPdf;

      /// <summary>
      /// referencia a la clase que gestiona el envio del PDF
      /// </summary>
      [SerializeField] private clsEnvioPdf refEnvioPdf;

      /// <summary>
      /// referencia a la clase que asigna todos los valores al PDF, nombre de usuario, nombre simulador etc
      /// </summary>
      [SerializeField, Header("Datos adicionales"), Space(10)]
      private ControladorValoresPDF refControladorValoresPDF;

      /// <summary>
      /// referencia a la clase que contiene los tiempos y intentos del usuario en la situacion seleccionada
      /// </summary>
      [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

      /// <summary>
      /// string del tiempo que se demoro el usuario en la situacion
      /// </summary>
      private string tiempoSituacion;

      /// <summary>
      /// referencia a la clase que contiene la calificacion de la evaluacion
      /// </summary>
      [SerializeField] private Evaluacion refEvaluacion;

      [SerializeField] public GameObject[] panelesHojasPDF;

      /// <summary>
      /// Codigo de la situacion desarrollada por el usuario
      /// </summary>
      public string codigoActividad;

      /// <summary>
      /// Nombre de la situacion que desarrollo el usuario
      /// </summary>
      public string nombreSituacion;

      #endregion

      #region accesors

      public string _codigoSituacion
      {
         set { codigoActividad = value; }
      }

      public string _nombreSituacion
      {
         set { nombreSituacion = value; }
      }

      #endregion

      #region public methods

      /// <summary>
      /// Metodo que asigna todos los valores al PDF y ejecuta la courutina para capturar todas las hojas del PDF
      /// </summary>
      public void CapturarImagenesPDF()
      {
         Debug.Log("CapturarImagenesPDF");

         refControladorValoresPDF.SetFechaInicio(DateTime.Today.ToString("dd/MM/yyyy"));
         refControladorValoresPDF.SetIntentos(refControladorDatosSesion._cantidadIntentos.ToString());
         refControladorValoresPDF.SetCalificacion(refEvaluacion.GetCalificacionTotalRango());
         refControladorValoresPDF.SetSituacion(DiccionarioIdiomas._instance.Traducir(nombreSituacion));

         var tmpDatosLogin = ClsSeguridad._instance.GetDatosSesion();

         refControladorValoresPDF.SetCurso(tmpDatosLogin[1]);
         refControladorValoresPDF.SetIDCurso(tmpDatosLogin[2]);
         refControladorValoresPDF.SetInstitucion(tmpDatosLogin[3]);
         refControladorValoresPDF.SetUsuario(tmpDatosLogin[4]);
         refControladorValoresPDF.SetUnidad(DiccionarioIdiomas._instance.Traducir("TextUnidadDescripcion")); //-------------------------------

         var tmpSegundos = Mathf.Floor(refControladorDatosSesion._tiempoSituacion % 60);
         var tmpMinutos = Mathf.Floor(refControladorDatosSesion._tiempoSituacion / 60);
         tiempoSituacion = (tmpMinutos < 10f? "0" + tmpMinutos : tmpMinutos.ToString()) + ":" + (tmpSegundos < 10f? "0" + tmpSegundos : tmpSegundos.ToString());
         tiempoSituacion = "00:" + tiempoSituacion;

         Debug.Log("este es el tiempo" + tiempoSituacion);
         refControladorValoresPDF.SetTiempoPractica(tiempoSituacion);

         if(couCapturarImagenPDF == null)
         {
            couCapturarImagenPDF = CouCapturarImagePDF();
            StartCoroutine(couCapturarImagenPDF);
         }
         else
         {
            StopCoroutine(couCapturarImagenPDF);
            couCapturarImagenPDF = CouCapturarImagePDF();
            StartCoroutine(couCapturarImagenPDF);
         }
      }

      #endregion

      #region courutines

      /// <summary>
      /// Courutina que activa cada hoja del PDF para tomarle una captura
      /// </summary>
      private IEnumerator CouCapturarImagePDF()
      {
         capturaHojasPdf = new Texture2D[panelesHojasPDF.Length];
         cameraCapturaPDF.gameObject.SetActive(true);
         yield return new WaitForSeconds(1);

         for(int i = 0; i < panelesHojasPDF.Length; i++)
         {
            panelesHojasPDF[i].SetActive(true);
            yield return new WaitForEndOfFrame();
            cameraCapturaPDF.Render();
            RenderTexture.active = renderTexturePDF;
            var tmpTexture2D = new Texture2D(824, 1079, TextureFormat.ARGB32, false, true);
            tmpTexture2D.ReadPixels(new Rect(0, 0, 824, 1079), 0, 0);
            tmpTexture2D.Apply();
            capturaHojasPdf[i] = tmpTexture2D;
            RenderTexture.active = null;
            panelesHojasPDF[i].SetActive(false);
         }

         cameraCapturaPDF.gameObject.SetActive(false);
         Debug.Log("codigo de la practica " + codigoActividad);
         refEnvioPdf.mtdDescargaryEnviarReporte(codigoActividad, refControladorDatosSesion._cantidadIntentos.ToString(), tiempoSituacion, refEvaluacion._refCalificacionSituacion2.GetCalificacionTotal(), capturaHojasPdf);
      }

      #endregion
   }
}