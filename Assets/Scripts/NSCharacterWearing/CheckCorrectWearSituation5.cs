﻿using NSActiveZones;
using NSBoxMessage;
using NSEvaluacion;
using NSInterfaz;
using NSSituacion5;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.UI;

namespace NSCharacterWearing
{
    public class CheckCorrectWearSituation5 : MonoBehaviour
    {
        #region members

        [SerializeField]
        private ControladorSituacion5 refControladorSituacion5;
                
        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField]
        private CheckCorrectTogglesActivate refCheckCorrectTogglesActivate;

        [SerializeField]
        private Button buttonAvatar;

        [SerializeField]
        private ActiveZonesController refActiveZonesController;

        [SerializeField]
        private Evaluacion refEvaluacion;

        private int intentosVestirPersonaje;
        #endregion

        #region public methods

        public void CorrectWear()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSiProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), Close);
            buttonAvatar.interactable = false;
            refControladorSituacion5._avatarVestido = true;
            refEvaluacion.AsignarCalificacionElementosProteccion(1f - Mathf.Clamp((0.1f * intentosVestirPersonaje), -1f, 0f));

            if (refControladorSituacion5._zonaSeguridadColocada)
                refActiveZonesController.DesactivateZone("senializacionSeguridad");
        }
               
        public void IncorrectWear()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeInsuficienteProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            intentosVestirPersonaje++;
            refControladorDatosSesion.AddIntentos();
        }

        public void Close()
        {
            PanelInterfazWearAvatar._instance.Mostrar(false);
        }
        
        public void OnButtonVerifyWearSituation()
        {
            refCheckCorrectTogglesActivate.VerifyGroupToggles("Situacion");
            Debug.Log("OnButtonVerifyWearSituation2");
        }

        public void ResetWearAvatar()
        {
            refCheckCorrectTogglesActivate.DesactivateAllToggles();
            refCheckCorrectTogglesActivate.ActiveGroupToggles("InitWear");
        }
        #endregion
    }
}
