﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace NSSituacion2
{
    public class ControladorConometro : MonoBehaviour
    {
        #region members

        private float tiempoActual;

        [SerializeField] private TextMeshProUGUI textTiempoConometro;

        private bool conometroCorriendo;
        #endregion

        public bool ConometroCorriendo
        {
            set { conometroCorriendo = value; }
        }

        // Update is called once per frame
        void Update()
        {
            ActualizarValorTiempoSesion();
        }

        public void ActualizarValorTiempoSesion()
        {
            if (!conometroCorriendo)
                return;

            tiempoActual += Time.deltaTime;
            var tmpSegundos = Mathf.Floor(tiempoActual % 60);
            var tmpMinutos = Mathf.Floor(tiempoActual / 60);
            textTiempoConometro.text = "00:"+((tmpMinutos < 10f) ? "0" + tmpMinutos.ToString() : tmpMinutos.ToString()) + ":" + ((tmpSegundos < 10f) ? "0" + tmpSegundos.ToString() : tmpSegundos.ToString());
        }

        public void OnButtonIniciarConometro()
        {
            tiempoActual = 0;
            conometroCorriendo = true;
        }
    } 
}
