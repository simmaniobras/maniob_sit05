﻿using NSBoxMessage;
using System.Collections;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSSituacion2
{
    public class ControladorPulsos : MonoBehaviour
    {
        #region members

        [SerializeField] private Image imagePulses;

        [SerializeField] private ControladorSituacion2 refControladorSituacion2;

        [SerializeField] private TMP_InputField inputSegundosPulsos;

        [SerializeField] private ControladorConometro refControladorConometro;

        private float cantidadPulsos;

        private bool contarPulso;

        #endregion

        #region methods

        public void PlayPruebaPulsos()
        {
            StopAllCoroutines();
            cantidadPulsos = refControladorSituacion2.CalcularCantidadPulsos();
            refControladorConometro.OnButtonIniciarConometro();
            StartCoroutine(CouPulsos());
        }

        public void StopPrueba()
        {
            StopAllCoroutines();
        }

        public void PreguntarEmpezarPruebaPulsos()
        {
            if (inputSegundosPulsos.text.Length > 0)            
                BoxMessageManager._instance.MtdCreateBoxMessageDecision("Ahora iniciará la prueba. Tenga en cuenta que debe realizar el conteo de pulsos en el macromedidor para aplicar las fórmulas.", "CANCELAR", "ACEPTAR", PlayPruebaPulsos);            
        }
        #endregion

        private IEnumerator CouPulsos()
        {
            var tmpSegundosPorPulso = refControladorSituacion2._cantidadSegundosPulsos / cantidadPulsos;
            Debug.Log("tmpSegundosPorPulso : " + tmpSegundosPorPulso);
            var tmpAngleAddPortion = 180f / tmpSegundosPorPulso;
            var tmpTiempoPrueba = 0f;
            var tmpAngle = 0f;

            if (refControladorSituacion2.CortaCircuitosAbiertos)
            {
                while (tmpTiempoPrueba <= refControladorSituacion2._cantidadSegundosPulsos)
                {
                    tmpTiempoPrueba += Time.deltaTime;
                    yield return null;
                }
                
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSeRegistraron") + " 0 " + DiccionarioIdiomas._instance.Traducir("mensajePulsosEnLaPrueba"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorConometro.ConometroCorriendo = false;
            }
            else
            {
                while (tmpTiempoPrueba <= refControladorSituacion2._cantidadSegundosPulsos)
                {
                    tmpTiempoPrueba += Time.deltaTime;
                    tmpAngle += tmpAngleAddPortion * Time.deltaTime;
                    tmpAngle = tmpAngle % 180f;
                    var tmpAlpha = Mathf.Sin(tmpAngle * Mathf.Deg2Rad) >= 0.85f ? Mathf.Sin(tmpAngle * Mathf.Deg2Rad) : 0;
                    imagePulses.color = new Color(imagePulses.color[0], imagePulses.color[1], imagePulses.color[2], tmpAlpha);
                    yield return null;
                }
                
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSeRegistraron") + (int) cantidadPulsos + DiccionarioIdiomas._instance.Traducir("mensajePulsosEnLaPrueba"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorConometro.ConometroCorriendo = false;
            }
        }
    }
}
