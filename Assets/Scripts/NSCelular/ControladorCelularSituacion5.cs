﻿using NSEvaluacion;
using System.Collections;
using NSSituacion5;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSCelular
{
   public class ControladorCelularSituacion5 : MonoBehaviour
   {
      #region members

      private int mensajesEnviados;

      private string circuitoID;

      private string transformadorID;

      private string apoyoID;

      [SerializeField] private ControladorConexionesMacroMedidor refControladorConexionesMacroMedidor;

      [SerializeField] private GameObject prefabMensajeUsuario;

      [SerializeField] private GameObject prefabMensajeCentroMando;

      [SerializeField] private Transform transformContentList;

      [SerializeField] private ScrollRect scrollRectCelular;

      [SerializeField] private TextMeshProUGUI textMensajeAEnviar;

      [SerializeField] private Evaluacion refEvaluacion;

      [SerializeField] private Button buttonEnviarMensaje;

      private float calificacionEnvioMensajes;

      #endregion

      private void OnEnable()
      {
         StopAllCoroutines();
         StartCoroutine(CouControladoraMensajes());
      }

      public void IniciarCelular()
      {
         circuitoID = "R" + Random.Range(0, 10000).ToString("X4");
         transformadorID = "T" + Random.Range(0, 10000).ToString("X4");
         apoyoID = "M" + Random.Range(0, 100000).ToString("X5");
      }

      public void OnButtonEnviarMensaje()
      {
         var tmpPrefabMensajeUsuario = Instantiate(prefabMensajeUsuario, transformContentList);
         tmpPrefabMensajeUsuario.GetComponent<SetMessage>().SetMensaje(textMensajeAEnviar.text);
         textMensajeAEnviar.text = "";
         LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
         scrollRectCelular.verticalNormalizedPosition = 0;
         mensajesEnviados++;
         Debug.Log("mensajesEnviados : " + mensajesEnviados);
         buttonEnviarMensaje.interactable = false;
      }

      public void SetNuevoMensajeUsuario()
      {
         textMensajeAEnviar.text = GetMensajeParaEnviar();
         buttonEnviarMensaje.interactable = true;
      }

      [ContextMenu("SetMensaje1")]
      public void SetMensaje1()
      {
         mensajesEnviados = 0;
         SetNuevoMensajeUsuario();
         calificacionEnvioMensajes = 0.25f;
      }

      [ContextMenu("SetMensaje2")]
      public void SetMensaje2()
      {
         mensajesEnviados = 2;
         SetNuevoMensajeUsuario();
         calificacionEnvioMensajes = 0.5f;
      }

      [ContextMenu("SetMensaje3")]
      public void SetMensaje3()
      {
         mensajesEnviados = 4;
         SetNuevoMensajeUsuario();
         calificacionEnvioMensajes = 0.75f;
      }

      [ContextMenu("SetMensaje4")]
      public void SetMensaje4()
      {
         mensajesEnviados = 6;
         SetNuevoMensajeUsuario();
         calificacionEnvioMensajes = 1f;
      }

      private string GetMensajeParaEnviar()
      {
         var tmpMensaje = "";

         if(mensajesEnviados == 0)
            tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar11") + circuitoID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar12") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar13") + apoyoID + ".";
         else if(mensajesEnviados == 2)
            tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar21") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar22");
         else if(mensajesEnviados == 4)
            tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar31") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar32");
         else if(mensajesEnviados == 6) tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar41") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Enviar42");

         return tmpMensaje;
      }

      private string GetMensajeCentroControl()
      {
         var tmpMensaje = "";

         if(mensajesEnviados == 1)
            tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control11") + circuitoID + ".";
         else if(mensajesEnviados == 3)
            tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control21") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control22");
         else if(mensajesEnviados == 5)
         {
            if(refControladorConexionesMacroMedidor._macromedidorConectado)
               tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control31");
            else
               tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control41");
         }
         else if(mensajesEnviados == 7) tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion5Control51");

         return tmpMensaje;
      }

      private IEnumerator CouControladoraMensajes()
      {
         while(true)
         {
            if(mensajesEnviados == 1 || mensajesEnviados == 3 || mensajesEnviados == 5 || mensajesEnviados == 7 || mensajesEnviados == 9)
            {
               yield return new WaitForSeconds(Random.Range(1f, 2f));
               var tmpPrefabMensajeCentroControl = Instantiate(prefabMensajeCentroMando, transformContentList);
               tmpPrefabMensajeCentroControl.GetComponent<SetMessage>().SetMensaje(GetMensajeCentroControl());
               LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
               refEvaluacion.AsignarCalificacionMensajesEnviados(calificacionEnvioMensajes);
               mensajesEnviados++;
            }

            yield return null;
            scrollRectCelular.verticalNormalizedPosition = 0;
         }
      }

      public void BorrarMensajes()
      {
         while(transformContentList.childCount > 0) DestroyImmediate(transformContentList.GetChild(0).gameObject);
      }
   }
}