﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GraficaPractica4 : MonoBehaviour {

    public LineRenderer lineaParaLaGrafica;
    public float AnchoImagen;
    public float AltoImagen;
    public float valorXmaximo;

    public TextMeshProUGUI textopotencia;

    // Use this for initialization
    void Start() {
       // Vector3[] puntos = new[] { new Vector3(1f, 1f, 0f), new Vector3(2f, 2f, 0f), new Vector3(3f, 3f, 0f) };
        //GenerarGrafica(puntos);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerarGrafica(Vector3[] punstosDelaGrafica)
    {

        lineaParaLaGrafica.SetPositions(punstosDelaGrafica);
        Debug.Log("se pinto la grafica");
    }

    public float[] ConvertirVectorAcordenadasDeLagrafica(float[] vector,float valorMaximo)
    {
        textopotencia.text= vector[19].ToString("#.#") + "kVA"; ;

        float[] aux = new float[vector.Length];
        for(int i=0; i< vector.Length;i++)
        {
            aux[i] = (vector[i] * AltoImagen) / valorMaximo;
            Debug.Log("valorX" + i + "= " + aux[i]);
        }
        return aux;
    }

    public float[] ValoresParaX(int tamaño)
    {
        float[] aux = new float[tamaño];// 24  
        
        for (int i = 0; i < tamaño; i++)// 0 - 24
        {            
            aux[i] += (i+1f) * AnchoImagen / 24f;// si 24 = 4.85f entonces i *4.85f / 24
            Debug.Log("valorY" + i + "= " + aux[i]);

            // 10 / 2 * 1 / 10 derecha a izquierda  
        }

        return aux;

    }


    public float[] ConvertirVectorSolucionNuevotrafo(float[] valoresDeEntrada, float valorMaximo, float porsentajeAReducir)
    {
        textopotencia.text = (valoresDeEntrada[19]/porsentajeAReducir).ToString("#.#")+"kVA";
        var NuevoValor = 0f;
        float[] aux = new float[valoresDeEntrada.Length];
        for (int i = 0; i < valoresDeEntrada.Length; i++)
        {
            NuevoValor = valoresDeEntrada[i] / porsentajeAReducir;
            aux[i] = (NuevoValor * AltoImagen) / valorMaximo;
            Debug.Log("valorX" + i + "= " + aux[i]);
        }
        return aux;
    }

}
